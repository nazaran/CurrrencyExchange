package com.hcl.ing.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.ing.model.TransactionInfo;
import com.hcl.ing.service.TransactionService;

@RestController
public class TransactionControll {

	@Autowired
	TransactionService transactionService;

	@RequestMapping("/hcl")
	public String display() {
		return "HCL";
	}

	@RequestMapping("/ccy ")
	public ResponseEntity<TransactionInfo> getCcy() {
		TransactionInfo txnInfo = new TransactionInfo();
		txnInfo.setFromCur("INR");
		txnInfo.setToCur("USD");
		return new ResponseEntity<>(txnInfo, HttpStatus.OK);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public TransactionInfo saveTransactionInfo(@RequestBody TransactionInfo transInfo) {
		transactionService.saveTransactionDetails(transInfo);
		System.out.println(transInfo.getUserId());
		return transInfo;
	}
	
	@RequestMapping(value="/display", method=RequestMethod.GET , produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<TransactionInfo>> displayTransactionDetail()
		{
			List<TransactionInfo> transInfo=transactionService.showTransactionDetails();
			if(transInfo.isEmpty())
			{
				return new ResponseEntity<List<TransactionInfo>>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<List<TransactionInfo>>(transInfo, HttpStatus.OK);
		}
		
	

}
