package com.hcl.ing.model;

import java.util.Date;

public class TransactionInfo {
	private Integer userId;
	private String fromCur;
	private String toCur;
	private double sell;
	private Date time;
	
	private String OriginCountry;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getFromCur() {
		return fromCur;
	}
	public void setFromCur(String fromCur) {
		this.fromCur = fromCur;
	}
	public String getToCur() {
		return toCur;
	}
	public void setToCur(String toCur) {
		this.toCur = toCur;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getOriginCountry() {
		return OriginCountry;
	}
	public void setOriginCountry(String originCountry) {
		OriginCountry = originCountry;
	}
	public double getSell() {
		return sell;
	}
	public void setSell(double sell) {
		this.sell = sell;
	}
	
}
