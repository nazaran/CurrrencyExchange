package com.hcl.ing.dao;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;


import com.hcl.ing.model.TransactionInfo;

@Repository
@Qualifier("transactionInfoDao")
public class TransactionInfoDaoImpl implements TransactionInfoDao {
	        
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	DataSource dataSource;

	
	@Override
	public void persistTransactioninfo(TransactionInfo transactionInfo) {
		String sql = "INSERT INTO transaction_details(user_id, from_cur, to_cur, sell, time, origin_country) values(?,?,?,?,?,?) ";
		// TODO Auto-generated method stub
		jdbcTemplate.update(sql,new Object[]{transactionInfo.getUserId(),transactionInfo.getFromCur(),transactionInfo.getToCur(),
				transactionInfo.getSell(),transactionInfo.getTime(),transactionInfo.getOriginCountry()
		});       
	}

	@Override
	public  List<TransactionInfo> getTransactionDetails() {
		// TODO Auto-generated method stub
	
		System.out.println("JdbcTemplate: " + jdbcTemplate);
		String sql = "select * from transaction_details";
		List<TransactionInfo> TransInfo=jdbcTemplate.query(sql,new BeanPropertyRowMapper(TransactionInfo.class));
		
		return TransInfo;
	}

}
