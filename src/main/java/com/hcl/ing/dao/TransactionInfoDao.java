package com.hcl.ing.dao;

import java.util.List;

import com.hcl.ing.model.TransactionInfo;

public interface TransactionInfoDao {
	public void persistTransactioninfo(TransactionInfo transactionInfo);
	public List<TransactionInfo> getTransactionDetails();

}
